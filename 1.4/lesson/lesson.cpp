//1.4 — A first look at functions and return values

#include <iostream>

int requestInt(){
	int value;
	std::cout << "Enter an integer: ";
	std::cin >> value;
	return value;
}

int main(){
	int x = requestInt();
	int y = requestInt();

	std::cout << x << " + " << y << " = " << x + y << std::endl;

	return 0;
}
