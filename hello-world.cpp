#include <iostream>

int main() {
	std::cout << "Hello World!" << std::endl;
	std::cout << "This is where I store all the programs I'll make to learn C++." << std::endl;
	std::cout << "I'm taking lessons from www.learncpp.com" << std::endl;
	std::cout << "Also, folders are organized according to leancpp's lessons." << std::endl;
	return 0;
}