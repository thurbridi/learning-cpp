//Questions: 4, 5.

#include <iostream>

int doubleNumber(int x){
	return 2 * x;
}

int main(){
	int number;
	std::cout << "Enter a number: ";
	std::cin >> number;
	std::cout << "WE DOUBLED IT! MAGIC!" << std::endl;
	std::cout << doubleNumber(number) << std::endl;

	return 0;
}