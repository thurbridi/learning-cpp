//1.4a — A first look at function parameters and arguments

#include <iostream>
#include <stdio.h>

int add(int a, int b){
	int sum = a + b;
	return sum;
}

int inputInt(){
	int x;
	std::cout << "Enter an integer: ";
	std::cin >> x;
	return x;
}

int main(){
	int x = inputInt();
	int y = inputInt();

	int sum;
	sum = add(x, y);

	std::cout << x << " + " << y << " = " << sum << std::endl;

	return 0;
}