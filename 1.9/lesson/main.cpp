// 1.9 - Header Files

#include <iostream>

// This includes the header file "add.h"
#include "add.h"

int main() {
	using namespace std;
	cout << "3 + 4 = " << add(3, 4) << endl;
	return 0;
}
