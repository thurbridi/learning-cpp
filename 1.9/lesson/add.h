// 1.9 - Header Files

// This is the start of the header guard.
#ifndef ADD_H
#define ADD_H

// This is the content of the .h file!
int add(int a, int b);

// This is the end of the header guard. :c
#endif