//1.3a — A first look at cout, cin, endl, 
//the std namespace, and using statements

#include <iostream>

int main(){
	using namespace std;
	int x;
	cout << "Enter a value for x: ";
	cin >> x;
	cout << "You entered: " << x << 	endl;
	return 0;
}