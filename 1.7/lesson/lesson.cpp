// 1.7 — Forward declarations

#include <iostream>

// This code will not compile without this line!
int add(int a, int b); 

int main() {
	std::cout << "The sum of 3 and 4 is: " << add(3, 4) 
						<< std::endl;

	return 0;
}

int add(int a, int b) {
	return a + b;
}
