// Question: 2

#include <iostream>

int doMath(int first, int second, int third, int fourth);

int main() {
	std::cout << "I'M DOING MATH: " << doMath(10, 5, 7, 2) 
						<< std:: endl;
	return 0;
}

int doMath(int first, int second, int third, int fourth) {
	return first + second * third / fourth;
}