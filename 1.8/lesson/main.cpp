// 1.8 - Programs with multiple files

#include <iostream>

int add(int a, int b);

int main() {
	using namespace std;
	cout << "3 + 4 = " << add(3, 4) << endl;
	return 0;
}
